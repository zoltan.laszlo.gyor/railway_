package rail;

import java.io.File;
import java.util.*;

public class RailMap {
    private String country;
    private LinkedList<Railway> railways;

    public RailMap(String country, String fileName) throws Exception{
        this.country = country;
        Scanner sc = new Scanner(new File(fileName));
        while(sc.hasNextLine()){
            Railway tmp = Railway.make(sc.nextLine());
            if (tmp != null){
                this.railways.add(tmp);
            }
        }
    }

    public LinkedList<String> getCities(){
        HashSet<String> set = new HashSet<>();
        for (Railway railway:this.railways) {
            set.add(railway.getCities()[0]);
            set.add(railway.getCities()[1]);
        }
        LinkedList<String> list = new LinkedList<>();

        for (String city: set) {
            list.add(city);
        }
        
        return  list;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("RailMap( " + this.country + ", [");
        for (Railway railway: this.railways) {
            sb.append(railway.toString());
            sb.append(",\n");
        }
        sb.append("])");
        return sb.toString();
    }

    public LinkedList<String> getNeighbours(String city){
        LinkedList<String> list = new LinkedList<>();
        for (Railway railway:this.railways) {
            if (railway.getOtherCity(city) != null) {
                list.add(railway.getOtherCity(city));
            }
        }
        return list;
    }

    public String getCapitalCity(){
        HashMap<String, Integer> map = new HashMap<>();
        LinkedList<String> cities = getCities();
        String capital = null;
        int max = 0;

        for (String city:cities) {
            for (Railway railway:this.railways) {
                if (railway.getOtherCity(city) != null) {
                    if (map.containsKey(city)) {
                        int value = map.get(city) + 1;
                        map.put(city, value);
                    } else {
                        map.put(city, 1);
                    }
                }
            }
            if(max < map.get(city)){
                capital = city;
                max = map.get(city);
            }
        }

        return capital;
    }
}
