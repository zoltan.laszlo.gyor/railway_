package rail;

public class Railway {
    private String city1;
    private String city2;
    private int distance;
    static Railway KESZTHELY_BUDAPEST = new Railway("Keszthely", "Budapest", 190);

    public Railway(String city1, String city2, int distance){
        this.city1 = city1;
        this.city2 = city2;
        this.distance = distance;
    }

    public static Railway make(String line){
        String[] data = line.split(" ");
        if (data.length <= 2){
            return null;
        }
        if (data[0] == null || data[1] == null){
            return null;
        }
        if (!Railway.isNumber(data[2])){
            return null;
        }
        return new Railway(data[0], data[1], Integer.parseInt(data[2]));
    }

    private static boolean isNumber(String number){
        try {
            Integer.parseInt(number);
            return true;
        }catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean hasEnd(String city){
        return city.equals(this.city1) || city.equals(this.city2);
    }

    public String getOtherCity(String city){
        if (city.equals(this.city1)) {
            return city2;
        }
        if (city.equals(this.city2)){
            return city1;
        }
        return null;
    }

    @Override
    public String toString(){ //???
        return "Railway(" + this.city1 + " - " + this.city2 + " " + this.distance + ").";
    }

    public String[] getCities(){
        return new String[]{city1, city2};
    }

    public int getDistance() {
        return distance;
    }
}
